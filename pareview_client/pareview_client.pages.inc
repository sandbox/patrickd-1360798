<?php
/**
 * @file
 * Pages of pareview_client module.
 */

/**
 * Page callback for PAReview result source form.
 */
function pareview_client_source_form() {
  $form = array();

  // Load the requested node source.
  $node = node_load(arg(1));
  $source = &$node->body['und'][0]['value'];

  // Add a textarea containing the source.
  $form['html_source'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTML Source'),
  );
  $form['html_source']['data'] = array(
    '#type' => 'textarea',
    '#default_value' => $source,
    '#rows' => 10,
  );

  return $form;
}

/**
 * Page callback for PAReview submission form.
 */
function pareview_client_submission_form() {
  $form = array();

  // Branch URL submission form.
  $form['branch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Review a branch'),
  );
  $form['branch']['repo_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL to git repository [optional: branch name]'),
    '#description' => variable_get('pareview_client_branch_url_descripton'),
    '#required' => TRUE,
  );
  $form['branch']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit branch'),
    '#validate' => array('pareview_client_submission_form_validate_branch'),
    '#submit' => array('pareview_client_submission_form_submit_branch'),
  );

  // Check for service maintenance.
  if (variable_get('pareview_client_maintenance', TRUE)) {
    // Disable submission on maintenance (except for admins).
    if (! user_access('configure pareview')) {
      $form['branch']['submit']['#disabled'] = TRUE;
    }
    // Display a status message.
    drupal_set_message(variable_get('pareview_client_maintenance_message'), 'warning', FALSE);
  }

  // Check wether we're servering a 'repeat' page.
  if (arg(2) === 'repeat') {
    // Get title of node to repeat reviewing.
    $select = db_select('node', 'n');
    $select->condition('n.nid', arg(1));
    $select->addField('n', 'title');
    $node_title = $select->execute()->fetchField();
    // Set the title (Repo URL) as default value.
    $form['branch']['repo_url']['#default_value'] = $node_title;
  }

  return $form;
}
