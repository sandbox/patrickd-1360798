<?php
/**
 * @file
 * Administration form for pareview_client settings.
 */

/**
 * Page callback for settings form.
 */
function pareview_client_settings_form($form, &$form_state) {
  $form = array();

  // Set vertical tabs.
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
  );

  // Server settings fieldset.
  $form['settings']['servers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server settings'),
  );
  $form['settings']['servers']['pareview_client_server_entry_points'] = array(
    '#type' => 'textarea',
    '#title' => t('Review servers'),
    '#description' => t('URL list of available review service entry points'),
    '#default_value' => variable_get('pareview_client_server_entry_points'),
  );
  // Username and password field for services authentification.
  $form['settings']['servers']['pareview_client_server_auth'] = array(
    '#type' => 'checkbox',
    '#title' => t('Authentification'),
    '#description' => t('Services authentification for entry points'),
    '#default_value' => variable_get('pareview_client_server_auth', FALSE),
  );
  $form['settings']['servers']['pareview_client_server_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('pareview_client_server_username'),
    '#states' => array(
      'visible' => array(
        // Hide if authentification checkbox is not ticked.
        'input[name="pareview_client_server_auth"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['servers']['pareview_client_server_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('pareview_client_server_password'),
    '#states' => array(
      'visible' => array(
        // Hide if authentification checkbox is not ticked.
        'input[name="pareview_client_server_auth"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Submission settings fieldset.
  $form['settings']['submission'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submission settings'),
  );
  $form['settings']['submission']['pareview_client_maintenance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Maintenance mode'),
    '#description' => t('Disables any new submissions'),
    '#default_value' => variable_get('pareview_client_maintenance', TRUE),
  );
  $form['settings']['submission']['pareview_client_maintenance_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Maintenance message'),
    '#description' => t('Message shown during maintenance'),
    '#default_value' => variable_get('pareview_client_maintenance_message'),
    '#states' => array(
      'visible' => array(
        // Hide if maintenance checkbox is not ticked.
        'input[name="pareview_client_maintenance"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['settings']['submission']['pareview_client_branch_url_descripton'] = array(
    '#type' => 'textfield',
    '#title' => t('Description of branch URL textfield'),
    '#maxlength' => 255,
    '#default_value' => variable_get('pareview_client_branch_url_descripton'),
  );
  $form['settings']['submission']['pareview_client_git_hosts'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed git hosts'),
    '#description' => t('List of allowed git hostnames'),
    '#default_value' => variable_get('pareview_client_git_hosts'),
  );

  // Processing settings fieldset.
  $form['settings']['processing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Processing settings'),
  );
  $form['settings']['processing']['pareview_client_error_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Error message'),
    '#description' => t('It will be shown on errors while processing'),
    '#default_value' => variable_get('pareview_client_error_message'),
  );
  $form['settings']['processing']['pareview_client_error_maintenance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Maintenance after a fatal error'),
    '#description' => t('Disable any new submissions after an error occourred'),
    '#default_value' => variable_get('pareview_client_error_maintenance', TRUE),
  );

  // Report settings fieldset.
  $form['settings']['report'] = array(
    '#type' => 'fieldset',
    '#title' => t('Report settings'),
  );
  $form['settings']['report']['pareview_client_report_footer'] = array(
    '#type' => 'textfield',
    '#title' => t('Footertext to add to the report'),
    '#default_value' => variable_get('pareview_client_report_footer'),
  );
  $form['settings']['report']['pareview_client_report_revisions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create a new node revision on changes.'),
    '#default_value' => variable_get('pareview_client_report_revisions', TRUE),
  );
  return system_settings_form($form);
}
