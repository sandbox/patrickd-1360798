This explains how to set up the service minimally and requires at least
  general knowledge about services, linux and drupal for shure.

  Requirements:
  - A public drupal site (service client)
  - A working PAReview.sh test environment, (server)

  The server site:
  - Install the pareview_server module on the site of your PAReview test
    environment and set up a services endpoint.
  - This service endpoint requires all PAReview resources and (if you want
    to use user authentification) also user.login resource.
  - If your using user authentification you have to create a user and gain
    him all required pareview permissions, otherwise you have to give
    the permissions to anonymous.
  - Create two directories "submissions" and "results" and give read and
    write permissions for all users to them. (This will be done more securely
    later.)
  - Move the pareview_server.sh script to your drupal root (where your
    pareview.sh should be) and start it (it will fork and run as deamon).

  The client site:
  - Install the pareview_client module on the drupal where you want to
    provide the online service.
  - Goto admin/config/system/pareview and configure the client.
    Paste in the entry points and, if used, configure the authentication
    settings. Set the allowed git hosts (eg. git.drupal.org). It's also
    required to get the service out of maintenance mode after you configured
    everything.
  - Configure the created PAReview Report content type.
  - Configure the user permissions for using the online service.

  Feel free to create an issue or contact me directly!
