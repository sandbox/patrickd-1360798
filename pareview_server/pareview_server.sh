#!/bin/bash

# This is kind of a ugly deamon for processing pareview submissions.
# It waits until a submission arrives in the submissions folder of
# the pareview_server module and pass it to the pareview.sh script.

# This file should be located at your drupal root, just like pareview.sh.

# Deamonize (start this script again, but dedicated).
if [ "x$1" != "x--" ]; then
  $0 -- 1> /dev/null 2> /dev/null &
  exit 0
fi

# Path to submissions directory.
submissions_dir="sites/all/modules/pareview_server/submissions"

while true
do
  # Check wether there are any files in the submission directory.
  # We get the oldest submission.
  submission_file=$(ls -1t $submissions_dir|tail -1)

  # Submissions directory is empty, so we wait for another submission.
  if [ -z "$submission_file" ]
  then
    submission_file=$(inotifywait -q -e create --format %f $submissions_dir)
  fi

  # Read the contained command arguments (their already escaped).
  command=$(cat "$submissions_dir/$submission_file")

  # Execute submission by pareview.sh.
  eval "./pareview.sh $command"

  # Delete submission file, so the server knows it's done.
  rm "$submissions_dir/$submission_file"

done
