<?php
/**
 * @file
 * Link pareview server functionalities to services module.
 *
 * NOTE:
 * The serverside implementation of this, including file handling,
 * is still dirty, I don't like it, but it works. -> TODO.
 */

/**
 * Callback to add a pareview submission to the local queue.
 */
function _pareview_server_add_submission($repo) {
  // We hash the repo URL to have a secure but explicit file.
  $repo_md5 = md5($repo);

  // Get path to the module's directory.
  $path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'pareview_server');

  // Assemble the command to execute securely.
  $command = escapeshellcmd($repo) . '>' . $path . '/results/' . $repo_md5;

  // Create a new file, containing the arguments for pareview.sh.
  // If the pareview_server.sh script is running correctly inotify
  // will trigger the review process.
  if (! file_exists($path . $repo_md5)) {
    file_put_contents($path . '/submissions/' . $repo_md5, $command);
  }
}

/**
 * Callback to retrieve the status of the pareview submission.
 */
function _pareview_server_retrieve_status($repo) {
  // Get path to submissions directory.
  $path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'pareview_server') . '/submissions/';

  // Check wether the submission file is stilly existing.
  if (file_exists($path . md5($repo))) {
    // Yep, so it's still queued.
    return 'queued';
  }
  else {
    // Nope, so it was processed and deleted.
    return 'done';
  }

  // TODO indicate that it is currently being processed.
}

/**
 * Callback to retrieve a pareview report.
 */
function _pareview_server_retrieve_report($repo) {
  // We hash the repo URL to have a secure but explicit file.
  $repo_md5 = md5($repo);

  // Get path to results directory.
  $path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'pareview_server') . '/results/';

  if (file_exists($path . $repo_md5)) {
    // The review's results where saved to a file so we read and return it.
    $result = file_get_contents($path . $repo_md5);
    // And delete it, so we don't waste space.
    drupal_unlink($path . $repo_md5);

    return $result;
  }
}
